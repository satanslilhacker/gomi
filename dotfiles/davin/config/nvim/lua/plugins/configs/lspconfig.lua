local lspconfig = require("lspconfig")

lspconfig.lua_ls.setup({
    settings = {
        Lua = {
            diagnostics = {
                globals = { "vim" },
                disable = { "different-requires" },
            },
        },
    },
})

lspconfig.rust_analyzer.setup({
    cargo = {
        features = { "all" },
    },
    filetypes = { "rust" },
})

lspconfig.gopls.setup({
    filetypes = { "go", "gomod", "gowork", "gotmpl" },
})

lspconfig.pyright.setup({
    filetypes = { "python" },
})

lspconfig.gleam.setup({
    filetypes = { "gleam" },
})

lspconfig.clangd.setup({
    filetypes = { "c", "cpp" },
})
